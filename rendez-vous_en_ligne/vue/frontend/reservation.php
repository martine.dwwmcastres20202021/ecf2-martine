<?php 
require ('./model/connect.php');
require ('./controller/error.php');
?>
<!DOCTYPE html>
<html lang="fr-FR" dir="ltr">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Rendez-vous en ligne</title>
  <link rel="stylesheet" href="./public/css/style.css">
</head>
<body>
  <header>
    <nav class="navbar">
      <div class="logo">
        <img src="./public/images/logo.png" alt="">
        <div class="title-site">hairfly</div>
      </div>
      <div class="menu-nav">
        <ul>
          <li><a href="../../vue/frontend/accueil.php" class="menu-item">Accueil</a></li>
          <li><a href="../../vue/frontend/reservation.php" class="menu-item">Rendez-vous</a></li>
          <li><a href="../../vue/frontend/admin.php" class="menu-item">Admin</a></li>
          
        </ul>
      </div>
    </nav>
  </header>
  <main>
    <div class="container-reservation" id="reservation">
    <h1>Réservation</h1>
    <section class="calendar"></section>
    <section class="reservation-card">
      <div class="card">
        <form class="form-card" action="./controller/data-reservation.php" method="POST" id="form-reservation">
          <div class="reservation-date">
            <div class="mois"></div>
            <div class="jour"></div>
            <div class="heure"></div>
          </div>
          <div class="input-date">
            <label for="date-reserv">Date</label>
            <input type="datetime-local" id="date-reserv" name="date-reserv" placeholder="Date" required>
          </div>
          <div class="input-lastname">
            <label for="lastname">Nom</label>
            <input type="text" id="lastname" name="lastname" placeholder="Nom" required>
          </div>
          <div class="input-firstname">
            <label for="firstname">Prénom</label>
            <input type="text" id="firstname" name="firstname" placeholder="Prénom" required>
          </div>
          <div class="input-phone">
            <label for="phone">Téléphone</label>
            <input type="text" id="phone" name="phone" placeholder="Téléphone" required>
          </div>
          <div  class="input-mail">
            <label for="mail">E-mail</label>
            <input type="email" id="mail" name="mail" placeholder="Mail" required>
          </div>
          <div class="btn-valid">
            <button type="submit" id="validate">Valider</button>
          </div>    
        </form>
        <!-- <?php 
      /*  if(isset($erreur)) {
          echo $erreur;
        }*/
        ?> -->
      </div>
    </section>
    </div>
  </main>
  <footer></footer>
  <script src="./public/js/script.js"></script>
</body>
</html>
