<?php 
require ('connect.php');
require ('error.php');
?>

<!DOCTYPE html>
<html lang="fr-FR">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Rendez-vous en ligne</title>
  <link rel="stylesheet" href="./../../public/css/style.css">
</head>
<body>
  <header>
    <nav class="navbar">
      <div class="logo">
        <img src="./public/images/logo.png" alt="">
        <div class="title-site">hairfly</div>
      </div>
      <div class="menu-nav">
        <ul>
          <li><a href="../../vue/frontend/accueil.php" class="menu-item">Accueil</a></li>
          <li><a href="../../vue/frontend/reservation.php" class="menu-item">Rendez-vous</a></li>
          <li><a href="../../vue/frontend/admin.php" class="menu-item">Admin</a></li>
          
        </ul>
      </div>
    </nav>
  </header>
  <main>
    <div class="container-calendar">
      <h1>Choisissez le jour et l'heure</h1>
    
  </main>
  <footer></footer>
  <script src="./public/js/script.js"></script>
</body>
</html>