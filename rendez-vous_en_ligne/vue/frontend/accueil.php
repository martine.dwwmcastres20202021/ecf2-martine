<!DOCTYPE html>
<html lang="fr-FR" dir="ltr">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Rendez-vous en ligne</title>
  <link rel="stylesheet" href="./public/css/style.css">
</head>
<body>
  <header>
    <nav class="navbar">
      <div class="logo">
        <img src="./public/images/logo.png" alt="">
        <div class="title-site">hairfly</div>
      </div>
      <div class="menu-nav">
        <ul>
          <li><a href="../../vue/frontend/accueil.php" class="menu-item">Accueil</a></li>
          <li><a href="../../vue/frontend/reservation.php" class="menu-item">Rendez-vous</a></li>
          <li><a href="../../vue/frontend/admin.php" class="menu-item">Admin</a></li>
          
        </ul>
      </div>
    </nav>
  </header>
  <main>
    <div class="container">
      <section class="accueil" id="accueil">
        <div class="img-wrapper">
         <img src="./public/images/bkg1.jpg" alt="">
        </div>
        <h1>Le Salon de coiffure Hairfly <span id="statut"></span></h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
          Voluptatem, culpa consequuntur! Architecto laborum eius cumque.</p>
          <div class="accueil-btn">
            <a href="reservation.php">Prise de rendez-vous</a>
          </div>
      </section>
      <!-- <section class="horaires" id="horaires"></section>
      <section class="reservation" id="reservation"></section> -->
    </div>
  </main>
  <footer></footer>
  <script src="./public/js/script.js"></script>
  <script src="./public/js/ouverture.js"></script>
</body>
</html>