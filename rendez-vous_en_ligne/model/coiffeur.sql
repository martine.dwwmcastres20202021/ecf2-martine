DROP DATABASE IF EXISTS coiffeur;
CREATE DATABASE coiffeur;
USE coiffeur;
CREATE TABLE clients(
    client_id INT PRIMARY KEY NOT NULL,
    lastname VARCHAR(100) NOT NULL,
    firstname VARCHAR(100) NOT NULL,
    phone VARCHAR(14) NOT NULL,
    mail VARCHAR (100) NOT NULL,
    date_reservation DATE NOT NULL,
    UNIQUE(date_reservation)
) ENGINE=InnoDB;


