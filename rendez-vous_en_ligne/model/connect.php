<?php 
    $servername = 'localhost';
    $dbname = 'coiffeur';
    $username = 'root';
    $password = '';
  
  
    //On essaie de se connecter
  try{
    $conn= new PDO("mysql:host=$servername ;dbname=$dbname;charset=utf8;", $username, $password);
    //On définit le mode d'erreur de PDO sur Exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);
    // echo 'Connexion réussie';
  }
  
  /*On capture les exceptions si une exception est lancée et on affiche
    *les informations relatives à celle-ci*/
  catch(PDOException $e){
    echo "Erreur : " . $e->getMessage();
  }

  //on ferme la connection
  $conn = null;
?>