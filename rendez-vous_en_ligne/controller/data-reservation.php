<?php 
require ('./model/connect.php');
require ('./controller/error.php');

$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$phone = $_POST['phone'];
$mail = $_POST['mail'];
$date_reservation = $_POST['date_reservation'];

if (isset($_POST['validate'])) {
  if (!empty($_POST['lastname']) AND !empty($_POST['firstname']) 
  AND  !empty($_POST['phone']) AND !empty($_POST['mail']) AND !empty($_POST['date_reservation'])) {
    $lastname = htmlspecialchars($_POST['lastname']);
    $firstname= htmlspecialchars($_POST['firstname']);
    $phone  = htmlspecialchars($_POST['phone']);
    $mail= htmlspecialchars($_POST['mail']);
    $date_reservation= htmlspecialchars($_POST['date_reservation']); 

    //inserer data client  
    $insererClient = $conn->prepare('INSERT INTO clients(lastname,firstname,phone,mail,date_reservation) VALUES (:lastname,:firstname,:phone,:mail,:date_reservation)');
    $insererClient->execute(array(
      ':lastname' => $lastname,
      ':firstname' => $firstname,
      ':phone' => $phone,
      ':mail' => $mail,
      ':date_reservation' => $date_reservation
    ));
    echo 'Le client a bien été ajouté !';
    } else {
      $erreur =  "Veuillez compléter tous les champs...";
    }
}

    //récupération data
    // $req = $conn->query('SELECT date_reservation DATE_FORMAT(date_reservation, \'%d/%m/%Y à %Hh%imin\') FROM reservations ORDER BY date_reservation DESC LIMIT 1');
?>
