// affichage ouverture/fermeture en couleur green/red

var d = new Date();
var n = d.getDay();
var now = d.getHours() + "." + d.getMinutes();
var weekdays = [
    ["Sunday"],
    ["Monday", 9.00, 12.00, 14.00,19.00],
    ["Tuesday", 9.00, 12.00, 14.00,19.00],
    ["Wednesday", 12.00,13.00, 14.00,19.00],//12.00,13.00
    ["Thursday", 9.00, 12.00, 14.00,19.00],
    ["Friday", 9.00, 11.30],
    ["Saturday", 9.00, 12.00, 14.00,19.00]
];
var day = weekdays[n];


if (now > day[1] && now < day[2] || now > day[3] && now < day[4]) {
    // console.log("Ouvert actuellement");
    document.getElementById('status').innerHTML = "Ouvert \nactuellement";
    document.getElementById('status').style.color ="green";

}
 else {
    // console.log("Fermé actuellement");
    document.getElementById('status').innerHTML = "Fermé \nactuellement";
    document.getElementById('status').style.color ="red";
}